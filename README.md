
# paragliding

sondrex-paragliding is an application designed to show data about igc files and allow the user to send said files to the system
The program is running at https://sondrex-paragliding.herokuapp.com/paragliding/api
Note on the code: I originally developed igcinfo using only one handler function, andh ave not had time to divide it up even though that would have been preferable over what I currently have.

The program stores both tracks and webhooks in the same database but at different collections

In all API's are the prerequisite adress, and in each example we have the method such as GET or POST, seperated with a space and then, if applicable, the remaining url in quotes

clock_trigger must be removed from the folders for tests to run and mongod must be running locally with the program being given access.

## Core API
For all use of core API the adress "<some_adress>/paragliding/api" comes before whatever is specified

GET will return how long the application have been running, info and the veGETrsion of the application in json.
Get "/track" will show you a list of all the id's of currently registered tracks in json
POST "/track" will allow you to post a link to an igc file. which it will attempt to read in and return the id in json formatting
GET "/track/<some_id>" will return detailed data about the track with the specified id in json
GET "/track/<some_id>/<field>" will return the specified field of the specified id in plain text
GET "/ticker/latest" will return the latest timestamp in plain text
GET "/ticker/" will return a json struct with the latest added timestamp, the first, the last stamp of a list of tracks, a list of tracks with default five(or however many elements are accessable) and the processing time it took to process it in milliseconds.
GET "/ticker/<timestamp>" will perform the same function as "/ticker/", however instead of giving the five(default value) first elements in the list of tracks it will take the five values that havee a timestamp larger then the one sent with.

All versions of ticker return the amount of time it took to perform the request in ms.

The program will also return appropriate error codes when an incorrect url have been given, you write an id that does not exist, etc

## Admin API
For all use of Admin API the adress "<some_adress>/Admin/api/tracks" comes before whatever is specified

GET "_count" returns the number of tracks in plain text
DELETE returns the number of deleted tracks and deletes all tracks in the database

## Webhook API
For all use of Webhook API the adress "<some_adress>/webhook/new_track/" comes before whatever is specified
When you add a new element it will check all webhooks to see if they should be updated, they will at most send 100 elements at once.
Each time a new track has been added it will notify the webhooks, the code uses a custom trigger handler with the three fields t_latest, tracks and processing, containing the latest timestamp, the list of tracks(although only up to 100) and the processing time.

POST Registers a new webhook for notifications about tracks being added to the system
GET "<webhook_id>" Gives information about a registered webhook
DELETE "<webhook_id>" Deletes the specified webhook

##clock trigger
On open stack a trigger which triggers every 10 minutes will update and check if the program have added anything, and if it has it will automatically send a message to this discord server: https://discord.gg/yRQfGEV

## Known bugs and missing features
- The first track added will not count for the ticker, it will start counting at 2, I have not yet found the reason for this
- Only two handler functions have been used, this made sense in the beginning but became more and more problematic as time went on, however I have not had time to divide it into multiple functions
- mlab does not allow my code to access the database, I have therefore been unable to store data on my database even though the tests works and localhost functins as expected
- clock_trigger has no implemented tests

## Repositories
https://Sondrex@bitbucket.org/Sondrex/clock_trigger.git
https://Sondrex@bitbucket.org/Sondrex/sondrex-paragliding

