package main

import (
	"gopkg.in/mgo.v2"
	"testing"
)

func setupDB(t *testing.T) *ParaglidingDB {
	db := ParaglidingDB{
		"mongodb://localhost",
		"testTrackDB",
		"Tracks",
		"trackid",
	}

	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()

	if err != nil {
		t.Error(err)
	}

	return &db
}

func tearDownDB(t *testing.T, db *ParaglidingDB) {
	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()

	if err != nil {
		t.Error(err)
	}

	err = session.DB(db.DatabaseName).DropDatabase()
	if err != nil {
		t.Error(err)
	}
}

func TestDBAdd(t *testing.T) {
	db := setupDB(t)
	defer tearDownDB(t, db)

	db.Init()
	if db.Count() != 0 {
		t.Error("Database not properly initialized, student count should be 0")
	}

	track := Track{1,
		TrackData{"A", "B", "C", "D", 10, ""},
		46466888}

	db.DBAdd(track)

	if db.Count() != 1 {
		t.Error("Adding new track failed")
	}
}

// GetTrack returns the specified track from the database and a bool stating if it were found or not
func TestGetTrack(t *testing.T) {
	db := setupDB(t)
	defer tearDownDB(t, db)

	db.Init()
	if db.Count() != 0 {
		t.Error("Database not properly initialized, student count should be 0")
	}

	track := Track{1,
		TrackData{"A", "B", "C", "D", 10, ""},
		46466888}
	db.DBAdd(track)

	if db.Count() != 1 {
		t.Error("Adding new track failed")
	}

	newTrack, ok := db.GetTrack(track.TrackID) // gets the data but it is empty

	if ok == -1 {
		t.Error("Couldn't find the added track")
	}

	if newTrack.Meta != track.Meta ||
		newTrack.Stamp != track.Stamp ||
		newTrack.TrackID != track.TrackID {
		t.Error("data do not match")
	}
}

// GetTicker is a function which returns a Ticker struct, it returns up to lenTracks tracks in a list greater then i
func TestDBGetTicker(t *testing.T) {
	db := setupDB(t)
	defer tearDownDB(t, db)

	db.Init()

	testData := TrackList{
		[]Track{
			{1, TrackData{"A", "B", "C", "D", 10, ""}, 1000},
			{2, TrackData{"E", "F", "G", "H", 20, ""}, 2000},
			{3, TrackData{"I", "J", "K", "L", 30, ""}, 3000},
			{4, TrackData{"M", "N", "O", "P", 40, ""}, 4000},
		},
	}

	if db.Count() != 0 {
		t.Error("Database not properly initialized, count should be 0")
	}

	// Adds four tracks to the database
	db.DBAdd(Track{1, TrackData{"A", "B", "C", "D", 10, ""}, 1000})
	db.DBAdd(Track{2, TrackData{"E", "F", "G", "H", 20, ""}, 2000})
	db.DBAdd(Track{3, TrackData{"I", "J", "K", "L", 30, ""}, 3000})
	db.DBAdd(Track{4, TrackData{"M", "N", "O", "P", 40, ""}, 4000})

	if db.Count() != 4 {
		t.Error("Adding new track failed")
	}

	testValue := db.DBGetTicker(1200, 2, 4) // should, among other things, get a list of tracks 1 and 2

	if testValue.Stop != 3000 {
		t.Error("Incorrect stop value!")
	} else if testValue.Start != 2000 {
		t.Error("Incorrect start value!")
	} else if testValue.Latest != 4000 {
		t.Error("Incorrect latest value!")
	} else if testValue.Tracks.TrackIds[0] != testData.TrackIds[1] {
		t.Error("Incorrect first value in array!")
	} else if testValue.Tracks.TrackIds[1] != testData.TrackIds[2] {
		t.Error("Incorrect second value in array!")
	} else { // checks if 4 is in list even though it shouldn't be
		v := testValue.Tracks.TrackIds
		for _, n := range v {
			if n.TrackID == 4 {
				t.Error("Element with ID 4 should not be present!")
			}
		}
	}
}

func TestDeleteAllTracks(t *testing.T) {
	db := setupDB(t)
	defer tearDownDB(t, db)

	db.Init()
	if db.Count() != 0 {
		t.Error("Database not properly initialized, student count should be 0")
	}

	db.DBAdd(Track{1, TrackData{"A", "B", "C", "D", 10, ""}, 1000})
	db.DBAdd(Track{2, TrackData{"E", "F", "G", "H", 20, ""}, 2000})

	if db.Count() != 2 {
		t.Error("Adding new track failed")
	}

	db.DeleteAllTracks()

	if db.Count() != 0 {
		t.Error("Deleting track failed")
	}
}

func TestAddWebhook(t *testing.T) {
	db := ParaglidingDB{
		"mongodb://localhost",
		"testTrackDB",
		"WebHooks",
		"idhook",
	}

	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()

	if err != nil {
		t.Error(err)
	}
	defer tearDownDB(t, &db)

	db.Init()
	if db.Count() != 0 {
		t.Error("Database not properly initialized, count should be 0")
	}

	hook := WebHookFull{Webhook{"", 1}, 1, 0}

	err = db.AddWebhook(hook)

	if db.Count() != 1 || err != nil {
		t.Error("Adding new hook failed!")
	}
}

func TestGetWebhook(t *testing.T) {
	db := ParaglidingDB{
		"mongodb://localhost",
		"testTrackDB",
		"WebHooks",
		"idhook",
	}

	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()

	if err != nil {
		t.Error(err)
	}
	defer tearDownDB(t, &db)
	db.Init()

	if db.Count() != 0 {
		t.Error("Database not properly initialized, count should be 0")
	}

	hook := WebHookFull{Webhook{"", 1}, 1, 0}

	err = db.AddWebhook(hook)

	if db.Count() != 1 || err != nil {
		t.Error("Adding new hook failed!")
	}

	hook2 := db.GetWebhook(1)

	if hook2.WHook != hook.WHook || hook2.IDHook == -1 {
		t.Error("The hook returned is incorrect!")
	}
}

func TestDeleteWebhook(t *testing.T) {
	db := ParaglidingDB{
		"mongodb://localhost",
		"testTrackDB",
		"WebHooks",
		"idhook",
	}

	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()

	if err != nil {
		t.Error(err)
	}
	defer tearDownDB(t, &db)
	db.Init()

	if db.Count() != 0 {
		t.Error("Database not properly initialized, count should be 0")
	}

	hook := WebHookFull{Webhook{"", 1}, 1, 0}

	err = db.AddWebhook(hook)

	if db.Count() != 1 || err != nil {
		t.Error("Adding new hook failed!")
	}

	if !db.DeleteWebhook(1) || db.Count() != 0 {
		t.Error("Deleting hook failed!")
	}
}

func TestGetWebHookID(t *testing.T) {
	db := ParaglidingDB{
		"mongodb://localhost",
		"testTrackDB",
		"WebHooks",
		"idhook",
	}

	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()

	if err != nil {
		t.Error(err)
	}
	defer tearDownDB(t, &db)
	db.Init()

	if db.Count() != 0 {
		t.Error("Database not properly initialized, count should be 0")
	}

	err = db.AddWebhook(WebHookFull{Webhook{"", 1}, 1, 0})
	if err != nil {
		t.Error("Adding new hook failed!")
	}

	err = db.AddWebhook(WebHookFull{Webhook{"", 1}, 2, 0})
	if err != nil {
		t.Error("Adding new hook failed!")
	}

	err = db.AddWebhook(WebHookFull{Webhook{"", 1}, 4, 0})
	if db.Count() != 3 || err != nil {
		t.Error("Adding new hook failed!")
	}

	// Infinite loop here!
	val := db.GetWebHookID()

	if val != 4 {
		t.Error("GetWebHookID returns the incorrect value!")
	}
}

func TestUpdateWebhooks(t *testing.T) {
	db := ParaglidingDB{
		"mongodb://localhost",
		"testTrackDB",
		"WebHooks",
		"idhook",
	}

	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()
	//
	if err != nil {
		t.Error(err)
	}
	defer tearDownDB(t, &db)

	db.Init()
	if db.Count() != 0 { // checks webhooks
		t.Error("Database not properly initialized, count should be 0")
	}

	// Add webhook
	hook := WebHookFull{Webhook{"https://discordapp.com/api/webhooks/505791012522950657/NojxOTTTVJ4yF9mYrpIEznzS-cGTA-_ceMSKKVaal5WgNej5Wk0YC2LjuS9TtLLx91zK", 1}, 1, 0}

	err = db.AddWebhook(hook)

	if db.Count() != 1 || err != nil {
		t.Error("Adding new hook failed!")
	}

	// Add track

	track := Track{1,
		TrackData{"A", "B", "C", "D", 10, ""},
		46466888}

	db.TrackCollectionName = "Tracks"
	db.TrackValue = "trackid"

	if db.Count() != 0 { // checks tracks
		t.Error("Database not properly initialized, count should be 0")
	}

	db.DBAdd(track)

	if db.Count() != 1 {
		t.Error("Adding new track failed")
	}

	// Check if webhook works
	err = db.UpdateWebhooks(track.Stamp)
	if err != nil {
		t.Error("Updating webhooks failed!")
	}
}
