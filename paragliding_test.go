package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

// Test for ReturnCorrectField
func TestReturnCorrectField(t *testing.T) {
	tT := Track{1, TrackData{"A", "B", "C", "D", 8.65, "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc"}, 1}

	// if any field is not returned correctly
	if tT.Meta.ReturnCorrectField("H_date") != "A" {
		t.Error("H_date is not returned correctly in ReturnCorrectField()!")
	} else if tT.Meta.ReturnCorrectField("pilot") != "B" {
		t.Error("pilot is not returned correctly in ReturnCorrectField()!")
	} else if tT.Meta.ReturnCorrectField("glider") != "C" {
		t.Error("glider is not returned correctly in ReturnCorrectField()!")
	} else if tT.Meta.ReturnCorrectField("glider_id") != "D" {
		t.Error("glider_id is not returned correctly in ReturnCorrectField()!")
	} else if tT.Meta.ReturnCorrectField("track_length") != "8.65" {
		t.Error("track_length is not returned correctly in ReturnCorrectField()!")
	} else if tT.Meta.ReturnCorrectField("track_src_url") != "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc" {
		t.Error("track_src_url is not returned correctly in ReturnCorrectField()!")
	}
}

// Test for ReturnTimeInCorrectFormat
func TestReturnTimeInCorrectFormat(t *testing.T) {
	timeRan := ReturnTimeInCorrectFormat(time.Duration(26)*time.Hour + time.Duration(68)*time.Second)
	if timeRan != "P0Y0M1DT2H1M8S" {
		t.Error("Wrong value returned!")
	}
}

// Test for CheckFirstElement
func TestCheckFirstElement(t *testing.T) {
	if !CheckFirstElements([]string{"", "paragliding", "api"}) {
		t.Error("The values are correct, CheckFirstElement should have returned true!")
	} else if CheckFirstElements([]string{"", "", ""}) {
		t.Error("CheckFirstElements should have returned false!")
	}
}

// Test for ReadURL
func TestReadURL(t *testing.T) {
	track := ReadURL(URL{"http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc"})
	// Needs to figure out what the website will return in values and replace temp values with this
	trackCheck := TrackData{
		"2016-02-19 00:00:00 +0000 UTC",
		"Miguel Angel Gordillo",
		"RV8", "EC-XLL",
		443.25735,
		"http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc",
	}

	if track != trackCheck {
		t.Error("ReadURL is not returning the correct values!")
	}
}

// Test for CheckForthElement
func TestCheckForthElement(t *testing.T) {
	if !CheckForthElement([]string{"", "", "", "track"}) {
		t.Error("forth element is 'track' and should have returned true!")
	} else if CheckForthElement([]string{"", "", "", ""}) {
		t.Error("forth element is blank and should not have returned true!")
	}
}

func TestParaglidingHandlerErrorCheck(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(ParaglidingHandler))
	defer ts.Close()

	resp, err := http.Get(ts.URL + "/paraglidin")
	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}

	if resp.StatusCode != http.StatusNotFound {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusNotFound, resp.StatusCode)
	}

	resp, err = http.Get(ts.URL + "/paragliding/a")
	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}

	if resp.StatusCode != http.StatusNotFound {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusNotFound, resp.StatusCode)
	}

	resp, err = http.Get(ts.URL + "/paragliding/api/t")
	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}

	if resp.StatusCode != http.StatusNotFound {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusNotFound, resp.StatusCode)
	}
}

func TestParaglidingHandlerApi(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(AdminHandler))
	defer ts.Close()

	resp, err := http.Get(ts.URL + "/paragliding/api")
	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusOK, resp.StatusCode)
	}

	resp, err = http.Get(ts.URL + "/paragliding/")
	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusOK, resp.StatusCode)
	}
}

func TestAdminHandler(t *testing.T) {
	Database = ParaglidingDB{
		"mongodb://localhost",
		"testTrackDB",
		"Tracks",
		"trackid",
	}
	Database.Init()

	ts := httptest.NewServer(http.HandlerFunc(AdminHandler))
	defer ts.Close()

	// Adds elements
	db := setupDB(t)
	defer tearDownDB(t, db)

	db.Init()
	if db.Count() != 0 {
		t.Error("Database not properly initialized, student count should be 0")
	}

	track := []Track{{1,
		TrackData{"A", "B", "C", "D", 10, ""},
		46466888}, Track{2,
		TrackData{"A", "B", "C", "D", 10, ""},
		46466888}}

	db.DBAdd(track[0])
	db.DBAdd(track[1])

	if db.Count() != 2 {
		t.Error("Adding new track failed")
	}

	// Checks AdminHandler
	client := &http.Client{}

	resp, err := http.Get(ts.URL + "/admin/api/tracks_count")

	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusOK, resp.StatusCode)
	}

	req, err := http.NewRequest(http.MethodDelete, ts.URL+"/admin/api/tracks", nil)
	if err != nil {
		t.Errorf("Error constructing the DELETE request, %s", err)
	}

	resp, err = client.Do(req)
	if err != nil {
		t.Errorf("Error executing the DELETE request, %s", err)
	}
	if resp.StatusCode != http.StatusOK {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusOK, resp.StatusCode)
	}

	resp, err = http.Get(ts.URL + "/admin/api/tr")
	if err != nil {
		t.Errorf("Error making the GET request, %s", err)
	}

	if resp.StatusCode != http.StatusNotFound {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusNotFound, resp.StatusCode)
		return
	} //

	resp, err = http.Get(ts.URL + "/admi/api/tracks_count")
	if err != nil {
		t.Errorf("Error making the GET request, %s", err)
	}

	if resp.StatusCode != http.StatusNotFound {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusNotFound, resp.StatusCode)
		return
	}
}
