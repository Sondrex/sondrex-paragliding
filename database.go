package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"time"
)

// Globaldb for TrackDatabase functions
var Globaldb TrackDatabase

// Init initializes the database
func (db *ParaglidingDB) Init() {
	session, err := mgo.Dial(db.DatabaseURL)

	if err != nil {
		panic(err)
	}
	defer session.Close()

	index := mgo.Index{
		Key:        []string{db.TrackValue},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}

	err = session.DB(db.DatabaseName).C(db.TrackCollectionName).EnsureIndex(index)

	if err != nil {
		panic(err)
	}
}

// Count returns number of elements in database
func (db *ParaglidingDB) Count() int {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	count, err := session.DB(db.DatabaseName).C(db.TrackCollectionName).Count()
	if err != nil {
		fmt.Printf("error in Count(): %v", err.Error())
		return -1
	}

	return count
}

// DBAdd adds a track to DB
func (db *ParaglidingDB) DBAdd(X Track) error {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.TrackCollectionName).Insert(X)
	if err != nil {
		fmt.Printf("error in insert(): %v", err.Error())
		return err
	}

	// Function to update webhooks
	return db.UpdateWebhooks(X.Stamp)
}

// GetTrack returns the specified track from the database and a bool stating if it were found or not
func (db *ParaglidingDB) GetTrack(keyID int) (Track, int) {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	track := Track{}
	allWasGood := keyID

	err = session.DB(db.DatabaseName).C(db.TrackCollectionName).Find(bson.M{db.TrackValue: keyID}).One(&track)

	if err != nil {
		allWasGood = -1
	}

	return track, allWasGood
}

// DBGetTicker is a function which returns a Ticker struct, it returns up to lenTracks tracks in a list greater then i
func (db *ParaglidingDB) DBGetTicker(i int64, l int, h int) Ticker {
	timeMeassure := time.Now()

	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	t := Ticker{}

	if h > 0 { // Checks if the highest id is higher then 0(which means there are elements in the list)
		tempTrack, _ := db.GetTrack(h) // list.TrackIds[h-1].Stamp
		t.Latest = tempTrack.Stamp

		// Value that limits max length based on actual number of elements
		length := l // Number of elements to display if the list is not too short
		start := 0

		for ; h-1 > start; start++ {
			tempTrack, _ = db.GetTrack(start)
			if i < tempTrack.Stamp { // if i is smaller then the selected value break from the loop
				break
			} else {
				length++ // Increases length with one per length of start increases
			}
		}

		// if there are fewer elements than we want
		if h < l {
			length = h + 1
		}

		// Sets values
		t.Tracks.TrackIds = []Track{}

		for v := start; v < length; v++ {
			tempTrack, _ = db.GetTrack(v)
			t.Tracks.TrackIds = append(t.Tracks.TrackIds, tempTrack)
		}

		// Start
		tempTrack, _ = db.GetTrack(start)
		t.Start = tempTrack.Stamp

		// Stop
		tempTrack, _ = db.GetTrack(length - 1)
		t.Stop = tempTrack.Stamp
	}

	timeComp2 := time.Now()
	t.Processing = float32(timeComp2.Sub(timeMeassure).Nanoseconds()) / 1000000 //

	return t
}

// DeleteAllTracks Deletes all tracks in database
func (db *ParaglidingDB) DeleteAllTracks() {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// session.DB(db.DatabaseName).C(db.TrackCollectionName).DropCollection()
	session.DB(db.DatabaseName).C(db.TrackCollectionName).RemoveAll(nil)

}

// AddWebhook gets specified webhook
func (db *ParaglidingDB) AddWebhook(v WebHookFull) error {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.TrackCollectionName).Insert(v)

	if err != nil {
		return err
	}
	return nil
}

// GetWebhook gets specified webhook. sets id of WebHookFull to -1 when not found
func (db *ParaglidingDB) GetWebhook(v int) WebHookFull {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	webHookFull := WebHookFull{}

	err = session.DB(db.DatabaseName).C(db.TrackCollectionName).Find(bson.M{"idhook": v}).One(&webHookFull)

	if err != nil {
		webHookFull.IDHook = -1
	}

	return webHookFull
}

// DeleteWebhook gets specified webhook, returns true if deletion were successful
func (db *ParaglidingDB) DeleteWebhook(v int) bool {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.TrackCollectionName).Remove(bson.M{"idhook": v})

	return err == nil
}

// GetWebHookID gets the current WebHookID
func (db *ParaglidingDB) GetWebHookID() int {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// Finds the last element by going through elements until the number of elements have been reached
	i := 1
	num := db.Count()

	for n := 1; i <= num; n++ {
		_, temp := db.GetTrack(n)
		if temp != -1 {
			i++
		}
	}

	web := WebHookFull{}
	err = session.DB(db.DatabaseName).C(db.TrackCollectionName).Find(bson.M{"idhook": i}).One(&web)

	return web.IDHook
}

// UpdateWebhooks updates the appropriate webhooks
func (db *ParaglidingDB) UpdateWebhooks(new int64) error {
	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// Goes through each and every element, WebHookID s the highest element
	for i := 1; i <= WebHookID; i++ {
		// Checks if element with id i exists
		timeMeassure := time.Now()

		hook := db.GetWebhook(i)
		if hook.IDHook != -1 {
			// Returns value with up to one hundred elements, as we want all of the elements
			ticker := db.DBGetTicker(hook.lastTrackID, 100, HighestID)
			// Gets number of elements larger then the one specified
			length := len(ticker.Tracks.TrackIds)

			// Checks if number of elements larger then the one specified is larger then minimum needed elements
			if length >= hook.WHook.MinTrigger {
				// Updates db.GetWebhook(i).lastTrackId to new
				err = session.DB(db.DatabaseName).C(db.TrackCollectionName).Update(
					bson.M{"idhook": i}, bson.M{"$set": bson.M{"lasttrackid": new}})
				if err != nil {
					return err
				}
				// code for sending t_latest, tracks and processing
				url := hook.WHook.WebhookURL

				// Gets passed time for this specific hook
				passedTime := float32(time.Now().Sub(timeMeassure).Nanoseconds()) / 1000000
				result, err := json.Marshal(ResponseBody{new, ticker.Tracks.TrackIds, passedTime})

				if err != nil {
					return err
				}

				_, err = http.Post(url, "application/json", bytes.NewBuffer(result))
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}
