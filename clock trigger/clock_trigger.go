package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

type WebValue struct {
	Content string `json:"content"`
}

// GetValue returns the latest timestamp from url
func GetValue(timestamp int64, url string) (int64, error) {
	response, err := http.Get(url)

	// If the url didn't work correctly
	if err != nil {
		return timestamp, err
	}
	defer response.Body.Close()

	// Problem reading the body of the website
	value, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return timestamp, err
	}

	latest, err := strconv.ParseInt(string(value), 10, 64)

	// the inpput is not an int
	if err != nil {
		return int64(0), err
	}

	return latest, err
}

// NotifyWebhook notifies the webhook
func NotifyWebhook(url string, timestamp int64, timeMeassure time.Time, newIsLarger bool) error {

	// Gets passed time for this specific hook
	passedTime := float32(time.Now().Sub(timeMeassure).Nanoseconds()) / 1000000

	var s string
	if newIsLarger {
		s = "larger"
	} else {
		s = "smaller"
	}
	s += " than the former value and took "
	s += fmt.Sprintf("%f", passedTime) + " milliseconds to get"


	result, err := json.Marshal(WebValue{
		"The latest timestamp in the database is " + strconv.FormatInt(timestamp, 10) + " which is " + s})

	if err != nil {
		return err
	}

	_, err = http.Post(url, "application/json", bytes.NewBuffer(result))
	if err != nil {
		return err
	}
	return nil
}

// main runs the primary code
func main() {
	t := time.Duration(600000000000) // 10 minutes in nanoseconds
	var timestamp int64
	timestamp = 0

	for {
		timeMeassure := time.Now()

		v, err := GetValue(timestamp, "https://sondrex-paragliding.herokuapp.com/paragliding/api/ticker/latest")

		// if the database is not empty and an error were found, panic
		if v != 0 && err != nil {
			panic(err)
		}

		fmt.Print(v) // DEBUG
		fmt.Print("\n") // DEBUG

		// if the stored timestamp is smaller then the latest timestamp returned
		if timestamp != v {
			err := NotifyWebhook("https://discordapp.com/api/webhooks/505791012522950657/NojxOTTTVJ4yF9mYrpIEznzS-cGTA-_ceMSKKVaal5WgNej5Wk0YC2LjuS9TtLLx91zK", v, timeMeassure, v > timestamp)
			fmt.Print("Test. yay\n") // DEBUG
			if err != nil {
				fmt.Printf("error in NotifyWebhook(): %v", err.Error())
			}
			timestamp = v
		}
		time.Sleep(t) // Sleeps for 10 minutes
	}
}
