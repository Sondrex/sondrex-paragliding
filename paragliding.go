package main

import (
	"encoding/json"
	"fmt"
	"github.com/marni/goigc"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// TrackDatabase Interface for functions in database
type TrackDatabase interface {
	Init()
	Count() int
	DBAdd(X Track) error
	GetTrack(keyID int) (Track, int)
	DBGetTicker(i int64, l int, h int) Ticker
	GetWebhook(v int) WebHookFull
	DeleteWebhook(v int) bool
	AddWebhook(v WebHookFull) error
	GetWebHookID() int
	UpdateWebhooks() error
}

// startingTime holds the starting time of the application
var startingTime time.Time

// HighestID is the highest id of a track currently registered
var HighestID int

// WebHookID is the highest id of a webhook currently registered
var WebHookID int

// LenTracks is the number of tracks to be returned at max when tracks change
var LenTracks int

// Database is the databse value for tracks(and tickers)
var Database ParaglidingDB

// webhookDB is the database for webhooks
var webhookDB ParaglidingDB

// ParaglidingDB holds database basic data
type ParaglidingDB struct {
	DatabaseURL         string
	DatabaseName        string
	TrackCollectionName string
	TrackValue          string
}

// TrackData is a struct containing data of a track(excluding id)
type TrackData struct {
	Hdate       string  `json:"H_date"`
	Pilot       string  `json:"pilot"`
	Glider      string  `json:"glider"`
	GliderID    string  `json:"glider_id"`
	TrackLength float32 `json:"track_length"`
	TrackURL    string  `json:"track_src_url"`
}

// APIInformation is a struct used to return info about the program
type APIInformation struct {
	Uptime  string `json:"uptime"`
	Info    string `json:"info"`
	Version string `json:"version"`
}

// Track is a struct containing id and track data as two seperate values
type Track struct {
	TrackID int       `json:"id"`
	Meta    TrackData `json:"meta"`
	Stamp   int64     `json:"timestamp"`
}

// TrackList holds a list of tracks
type TrackList struct {
	TrackIds []Track `json:"tracks"`
}

// URL is a string which is refered to as "url" in json
type URL struct {
	URL string `json:"url"`
}

// Ticker is a struct which stores ticker data
type Ticker struct {
	Latest     int64     `json:"t_tatest"`
	Start      int64     `json:"t_start"`
	Stop       int64     `json:"t_stop"`
	Tracks     TrackList `json:"tracks"`
	Processing float32   `json:"processing"`
}

// Webhook holds data about a webhook
type Webhook struct {
	WebhookURL string `json:"webhookURL"`
	MinTrigger int    `json:"minTriggerValue"`
}

// WebHookFull contains the webhook and its id
type WebHookFull struct {
	WHook       Webhook
	IDHook      int
	lastTrackID int64
}

// Init initiates the in-memory storage
func (t *TrackList) Init() {
	t.TrackIds = []Track{}
}

// ResponseBody is a struct used for returning data to a webhooh
type ResponseBody struct {
	Last       int64   `json:"t_latest"`
	Tracks     []Track `json:"tracks"`
	Processing float32 `json:"processing"`
}

// ReturnCorrectField returns value in field x(field is in json format)
func (t TrackData) ReturnCorrectField(x string) string {
	// either pilot, glider, glider_id, track_length, H_date
	/*
		temp := ""
		json.Unmarshal([]byte(x), temp)
	*/
	switch x {
	case "pilot":
		return t.Pilot
	case "glider":
		return t.Glider
	case "glider_id":
		return t.GliderID
	case "track_length":
		return strconv.FormatFloat(float64(t.TrackLength), 'f', -1, 32)
	case "H_date":
		return t.Hdate
	case "track_src_url":
		return t.TrackURL
	}
	return ""
}

// CheckFirstElements checks if the second and third elements of the URL are "paragliding" and "api"
func CheckFirstElements(parts []string) bool {
	return parts[1] == "paragliding" && parts[2] == "api"
}

// CheckForthElement checks if the forth element in the URL is an ecceptable element
func CheckForthElement(parts []string) bool {
	return parts[3] == "track" || parts[3] == "ticker" || parts[3] == "webhook"
}

// errorCheck runs error checks for finding a 404 error where 406 cannot occur
func errorCheck(w http.ResponseWriter, parts []string) bool {
	var val bool

	if len(parts) > 3 {
		val = CheckFirstElements(parts) && CheckForthElement(parts)
	} else {
		val = CheckFirstElements(parts)
	}

	return val
}

// ReturnTimeInCorrectFormat returns in format specified in ISO 8601
func ReturnTimeInCorrectFormat(t time.Duration) string {
	years := int(t.Hours() / 24 / 365.2425)
	months := int(t.Hours() / 24 / (365.2425 / 12))
	days := int(t.Hours() / 24)

	return fmt.Sprintf("P%vY%vM%vDT%vH%vM%vS",
		years,
		months%12,
		days%366,
		int(t.Hours())%24,
		int(t.Minutes())%60,
		int(t.Seconds())%60)
}

// ReadURL reads a track from an URL
func ReadURL(url URL) TrackData { // Must get a TrackData from an URL
	s := url.URL // set s to the url given by the user

	track, err := igc.ParseLocation(s)
	if err != nil {
		// Error("Problem reading track", err) // Temporearilly disabled
	}
	track.Task.Start = track.Points[0]
	track.Task.Finish = track.Points[len(track.Points)-1]
	track.Task.Turnpoints = track.Points[1 : len(track.Points)-2]

	// fmt.Print(url.URL) // DEBUG

	return TrackData{track.Date.String(), track.Pilot, track.GliderType, track.GliderID, float32(track.Task.Distance()), url.URL}
	//return TrackData{} // DEBUG
}

// ParaglidingHandler is the handler of the url
func ParaglidingHandler(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/")

	// Redirects url if needed
	if len(parts) == 3 && parts[2] == "" {
		http.Redirect(w, r, parts[0]+"/"+parts[1]+"/api", http.StatusSeeOther)
	}

	if !errorCheck(w, parts) {
		http.Error(w, "404 page not found", http.StatusNotFound)
	} else {
		switch len(parts) {
		case 3: // paragliding/api
			// Gets the number of seconds the program have run
			t := time.Now()
			transTime := t.Sub(startingTime)

			info := APIInformation{ReturnTimeInCorrectFormat(transTime), "Service for paragliding tracks.", "v1"}

			// json.NewEncoder(w).Encode(info)
			js, err := json.Marshal(info)

			if err != nil {
				http.Error(w, fmt.Sprintf("Mershal error: %s", err), http.StatusInternalServerError)
			} else {
				w.Header().Set("Content-Type", "application/json")
				w.Write(js)
			}
			break
		case 4: // paragliding/api/track
			switch r.Method {
			case "POST": // Track registration, asks for url to template and returns id
				if parts[3] == "track" {
					var u URL
					var p TrackData

					err := json.NewDecoder(r.Body).Decode(&u) // json.NewDecoder(r.Body).Decode(&p)

					if err != nil {
						// fmt.Print(err.Error())
						http.Error(w, err.Error(), http.StatusNotAcceptable)
					} else { // succsess, add track
						p = ReadURL(u)

						HighestID++

						// Adds itentified track to database
						err = Database.DBAdd(Track{HighestID, p, time.Now().UnixNano() / 1000000})
						if err != nil {
							http.Error(w, fmt.Sprintf("Error adding element to database: %s", err), http.StatusInternalServerError)
						}

						value, ok := Database.GetTrack(HighestID)
						if ok == -1 {
							http.Error(w, "Error finding added element in database", http.StatusInternalServerError)
						}

						js, err := json.Marshal(value.TrackID)

						if err != nil {
							http.Error(w, fmt.Sprintf("Mershal error: %s", err), http.StatusInternalServerError)
						} else {
							w.Header().Set("Content-Type", "application/json")
							w.Write(js)
							// json.NewEncoder(w).Encode(highestID)
						}
					}
				} else {
					http.Error(w, "Error 404 page not found", http.StatusNotFound)
				}
				break
			case "GET": // Returns the array with all tracks ids
				// Array with all ids
				if parts[3] == "track" { // api/track
					list := []int{}
					for i := 1; i <= HighestID; i++ {
						temp, ok := Database.GetTrack(i)
						if ok == -1 {
							http.Error(w, "Error finding element in database", http.StatusInternalServerError)
						}
						list = append(list, temp.TrackID)
					}

					// fmt.Fprint(w, a)
					js, err := json.Marshal(list)

					if err != nil {
						http.Error(w, fmt.Sprintf("Mershal error: %s", err), http.StatusInternalServerError)
					} else {
						w.Header().Set("Content-Type", "application/json")
						w.Write(js)
					}
				} else {
					http.Error(w, "Error 404 page not found", http.StatusNotFound)
				}
				break
			}
			break
		case 5: // paragliding/api/track/<id> or paragliding/api/ticker/(latest or <timestamp> or "")
			if parts[3] == "track" { // track
				value, err := strconv.Atoi(parts[4])

				// Gets the value in question
				track, x := Database.GetTrack(value)

				// Checks if there is an error or if the value was not found
				if err != nil {
					http.Error(w, "Error 406: "+parts[4]+" is not a valid id", http.StatusNotAcceptable)
				} else if x == -1 {
					http.Error(w, "Error 404 page not found", http.StatusNotFound)
				} else {
					// json.NewEncoder(w).Encode(trackList.TrackIds[x - 1].Meta)
					js, err := json.Marshal(track.Meta)

					if err != nil {
						http.Error(w, fmt.Sprintf("Mershal error: %s", err), http.StatusInternalServerError)
					} else {
						//give json to customer
						w.Header().Set("Content-Type", "application/json")
						w.Write(js)
					}
				}
			} else if parts[3] == "ticker" { // ticker
				if parts[4] == "latest" { // latest
					track, ok := Database.GetTrack(HighestID)

					if ok == -1 {
						http.Error(w, "Could not find any tickers in database", http.StatusInternalServerError)
					}

					js, err := json.Marshal(track.Stamp)

					if err != nil {
						http.Error(w, fmt.Sprintf("Mershal error: %s", err), http.StatusInternalServerError)
					} else {
						w.Header().Set("Content-Type", "text/plain")
						w.Write(js)
					}
				} else if parts[4] == "" { // empty, counts as zero
					t := Database.DBGetTicker(0, LenTracks, HighestID) // gets ticker from the start

					js, err := json.Marshal(t)

					if err != nil {
						http.Error(w, fmt.Sprintf("Mershal error: %s", err), http.StatusInternalServerError)
					} else {
						//give json to customer
						w.Header().Set("Content-Type", "application/json")
						w.Write(js)
					}
				} else { // <timestamp>
					_, err := strconv.Atoi(parts[4]) // checks if parts[4] is an actual number
					value, err := strconv.ParseInt(parts[4], 10, 64)

					js, _ := json.Marshal(Database.DBGetTicker(value, LenTracks, HighestID))

					// checks if an id have indeed been read
					if err != nil {
						http.Error(w, "Error 406: "+parts[4]+" is not a valid id", http.StatusNotAcceptable)
					} else {
						w.Header().Set("Content-Type", "application/json")
						w.Write(js)
					}
				}
			} else {
				http.Error(w, "Error 404 page not found", http.StatusNotFound)
			}
			break
		case 6: // api/track/<id>/<field> || api/webhook/new_track/("" || <webhook_id>)
			if parts[3] == "track" {
				value, err := strconv.Atoi(parts[4])

				// Gets the value in question
				track, x := Database.GetTrack(value)

				// Checks if a valid input is found and returns an appropriate error code if not
				if err != nil {
					http.Error(w, "Error 406: "+parts[4]+" is not a valid id", http.StatusNotAcceptable)
				} else if x == -1 {
					http.Error(w, "Error 404 page not found", http.StatusNotFound)
				} else {
					temp := track.Meta.ReturnCorrectField(parts[5])

					if temp != "" {
						// json.NewEncoder(w).Encode(temp)
						js, err := json.Marshal(temp)

						if err != nil {
							http.Error(w, fmt.Sprintf("Mershal error: %s", err), http.StatusInternalServerError)
						} else {
							w.Header().Set("Content-Type", "text/plain")
							w.Write(js)
						}

					} else {
						http.Error(w, "404 page not found", http.StatusNotFound)
					}
				}
			} else if parts[3] == "webhook" && parts[4] == "new_track" {
				if parts[5] == "" {
					if r.Method == "POST" { // Register new webhook
						var web Webhook

						err := json.NewDecoder(r.Body).Decode(&web)

						if err != nil {
							http.Error(w, fmt.Sprintf("Mershal error: %s", err), http.StatusNotAcceptable)
						} else { // web is the new webhook
							if web.MinTrigger <= 0 || web.MinTrigger > 100 {
								web.MinTrigger = 1
							}
							// add element
							WebHookID++
							// gets the stamp of the latest added id
							t, _ := Database.GetTrack(HighestID)
							err = webhookDB.AddWebhook(WebHookFull{web, WebHookID, t.Stamp})

							if err != nil {
								http.Error(w, fmt.Sprintf("Failed to add webhook: %s", err), http.StatusInternalServerError)
								WebHookID--
							} else {
								js, err := json.Marshal(WebHookID)

								if err != nil {
									http.Error(w, fmt.Sprintf("Mershal error: %s", err), http.StatusInternalServerError)
								} else {
									w.Header().Set("Content-Type", "text/plain")
									w.Write(js)
								}
							}
						}
					} else {
						http.Error(w, "404 page not found", http.StatusNotFound)
					}
				} else { // GET or DELETE <webhook_id>
					value, err := strconv.Atoi(parts[5])

					if err != nil {
						http.Error(w, "Error 406: "+parts[5]+" is not a valid id", http.StatusNotAcceptable)
					} else {
						hook := webhookDB.GetWebhook(value)

						if hook.IDHook != -1 {
							if r.Method == "GET" { // Get specified webhook
								// return hook as specified
								js, err := json.Marshal(hook.WHook)

								if err != nil {
									http.Error(w, fmt.Sprintf("Mershal error: %s", err), http.StatusInternalServerError)
								} else {
									w.Header().Set("Content-Type", "application/json")
									w.Write(js)
								}
							} else if r.Method == "DELETE" { // Delete specified webhook
								deleted := webhookDB.DeleteWebhook(hook.IDHook)
								if !deleted {
									http.Error(w, "Deletion failed", http.StatusInternalServerError)
								} else {
									// return hook as specified
									js, err := json.Marshal(hook.WHook)

									if err != nil {
										http.Error(w, fmt.Sprintf("Mershal error: %s", err), http.StatusInternalServerError)
									} else {
										w.Header().Set("Content-Type", "application/json")
										w.Write(js)
									}
								}
							} else {
								http.Error(w, "404 page not found", http.StatusNotFound)
							}
						} else {
							http.Error(w, "Error 406: webhook with id "+parts[5]+" is not in database", http.StatusNotAcceptable)
						}
					}
				}
			} else { // Not a valid address
				http.Error(w, "404 page not found", http.StatusNotFound)
			}
			break
		default: // Default error when none of the above is true
			http.Error(w, "404 page not found", http.StatusNotFound)
			break
		}
	}
}

// AdminHandler handles all requests regarding admin
func AdminHandler(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/")
	if len(parts) == 4 { // GET /admin/api/tracks_count or DELETE /admin/api/tracks
		if parts[1] == "admin" && parts[2] == "api" {
			if parts[3] == "tracks_count" && r.Method == "GET" { // Returns number of tracks in the database
				js, err := json.Marshal(Database.Count())

				if err != nil {
					http.Error(w, fmt.Sprintf("Mershal error: %s", err), http.StatusInternalServerError)
				} else {
					w.Header().Set("Content-Type", "text/plain")
					w.Write(js)
				}
			} else if parts[3] == "tracks" && r.Method == "DELETE" {
				startNum := Database.Count()

				fmt.Print("Test\n")

				Database.DeleteAllTracks()

				num := startNum - Database.Count()

				js, err := json.Marshal(num)

				if err != nil {
					http.Error(w, fmt.Sprintf("Mershal error: %s", err), http.StatusInternalServerError)
				} else {
					//give json to customer
					HighestID = 0
					w.Header().Set("Content-Type", "text/plain")
					w.Write(js)
				}
			} else { // The adress does not exist
				http.Error(w, "404 page not found", http.StatusNotFound)
			}
		} else {
			http.Error(w, "404 page not found", http.StatusNotFound)
		}
	}
}

// main is where the code starts
func main() {
	// Starts the timer to check how long the program have been running
	startingTime = time.Now()
	LenTracks = 5

	// Database of tracks
	Database = ParaglidingDB{
		// "mongodb://localhost", // localhost alternative
		"mongodb://valor:aaaa11@ds139992.mlab.com:39992/sondrex-paragliding",
		"sondrex-paragliding",
		"paraglidngTracks",
		"trackid",
	}

	// Database of webhooks
	webhookDB = ParaglidingDB{
		// "mongodb://localhost", // localhost alternative
		"mongodb://valor:aaaa11@ds139992.mlab.com:39992/sondrex-paragliding",
		"sondrex-paragliding",
		"paraglidngWebhooks",
		"idhook",
	}
	// Sets highest id to the actual value it should be
	Database.Init()
	webhookDB.Init()

	// Gets the highest id for both collections
	HighestID = Database.Count()
	WebHookID = webhookDB.GetWebHookID()

	// Sets the port as what it is assigned to be or 8080 if none is found
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	// The code that actually
	http.HandleFunc("/paragliding/", ParaglidingHandler)
	http.HandleFunc("/admin/", AdminHandler)
	http.ListenAndServe(":"+port, nil) // https://sondrex-paragliding.herokuapp.com:80
}
